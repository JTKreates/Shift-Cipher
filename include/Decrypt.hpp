#include <iostream>
#include <fstream>

class Decrypt
{
public:
    Decrypt();
    int start();

private:
    std::ifstream inputStream;

    std::string message;
    std::string filename;
    int shift;
    char choice;
};

#include <iostream>
#include <fstream>

class Encrypt
{
public:
    Encrypt();
    int start();

private:
    std::ifstream inputStream;

    std::string message;
    std::string filename;
    int shift;
    char choice;
};

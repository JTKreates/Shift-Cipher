# Shift Cipher
## Programmed by JTKreates
-----

## About

This project is just a simple shift cipher programmed in c++.
It has basic functionality so you can choose whether you want to encrypt or decrypt a message.
You can also to choose to import the message you want to encrypt/decrypt from a text file.
Your encrypted/decrypted message will also be saved to a file labelled OUTPUT.txt.

## Compiling

To compile it just copy and pase `src` and `include` folders into your project source and include 
them in your project. You can also build them from command line if you know how to do it.

A Makefile will be created soon for easy compiling.

If you get any errors please leave an issue in the issues tab.

## Download

Check out the [releases](https://github.com/JTKreates/Shift-Cipher/releases) page to download the windows binary
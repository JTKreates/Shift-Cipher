#include "Decrypt.hpp"

Decrypt::Decrypt() {}

int Decrypt::start()
{
    std::ofstream outputStream("OUTPUT.txt", std::ofstream::out);
    std::cout << "What shift do you want to use?" << std::endl;
    std::cin >> shift;
    shift %= 26;
    std::cout << "Do you want to import text from an external file? (y/n)" << std::endl;
    std::cin >> choice;
    if(choice == 'y' || choice == 'Y')
    {
        std::cout << "What is the name of the text file?" << std::endl;
        std::cin >> filename;
        size_t a = filename.find(".txt");
        if(a == std::string::npos)
            filename += ".txt";
        inputStream.open(filename);
        if(!inputStream.is_open())
        {
            std::cout << "Error: Could not open file " << filename << std::endl;
            return -1;
        }
        getline(inputStream, message);
    }
    else
    {
        std::cout << "Enter the message you want to decrypt" << std::endl;
        ws(std::cin);
        getline(std::cin, message);
    }

    for(int i = 0; i < message.length(); i++)
    {
        if(message[i] >= 'a' && message[i] <= 'z')
        {
            if((message[i] - shift) >= 'a')
                message[i] -= shift;
            else
                message[i] = (message[i] - shift) - 'a' + 'z';
        }
        if(message[i] >= 'A' && message[i] <= 'Z')
        {
            if((message[i] - shift) >= 'A')
                message[i] -= shift;
            else
                message[i] = (message[i] - shift) - 'A' + 'Z';
        }
    }
    outputStream << message << "\n";

    std::cout << "\nDecrypted message is " << std::endl;
    std::cout << message << std::endl;
    std::cout << "\nLook for a file named OUTPUT.txt to find your decrypted message" << std::endl;
    return 0;
}

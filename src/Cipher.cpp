#include "Cipher.hpp"

Cipher::Cipher() {}

int Cipher::run()
{
    Encrypt e;
    Decrypt d;
    std::cout << "Shift Cipher" << std::endl;
    std::cout << "1) Encrypt a message" << std::endl;
    std::cout << "2) Decrypt a message" << std::endl;
    std::cout << "3) Exit" << std::endl;
    std::cin >> sel;

    switch(sel)
    {
    case 1:
        e.start();
        break;
    case 2:
        d.start();
        break;
    case 3:
        return 0;
    default:
        return -1;
    }
    return 0;
}
